cmake_minimum_required(VERSION 3.25)
project(ConvexHull)

set(CMAKE_CXX_STANDARD 17)

include_directories(.)

add_executable(ConvexHull
        Chan-source.cpp
        Chen-Liu-point-source.h
        Chen-Liu-source.cpp
        Chen-Liu-source.h
        convex_hull_xy.c
        ConvexHull_TSE.cpp
        csv_manipulations.hpp
        tse.hpp)
