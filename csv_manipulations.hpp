#pragma once

#include <vector> // std::vector
#include <limits> // std::numeric_limits<int>::max()
//#include <iostream> // std::ostream
#include <fstream> // std::ifstream
#include <sstream> // std::stringstream
#include <string> // std::getline
#include <tuple> // std::tuple
#include <stdexcept> // std::runtime_error


/* reads numerical-only CSV data from a file to a vector of vectors: every row in the file is mapped to a row in the outer vector. Rows of different lengths are supported though this feature is not used in the convex hull routine in the other way than to check the file format. Return value is a triplet of number of rows and minimal/maximal numbers of columns */
template <typename fp_t> std::tuple<int, int, int> read_csv_without_column_names(
std::string file_name, // file name to read CSV data from
std::vector<std::vector<fp_t> > &a) // [I] empty vector of vectors to fill with the data
{
std::string line;
int i_row=0, n_cols, n_cols_min=std::numeric_limits<int>::max(), n_cols_max=0;
fp_t number; // where to read every numerical token

std::ifstream in_file(file_name);
if (!in_file.is_open()) throw std::runtime_error("Could not open file");
while(std::getline(in_file, line)) // read line-by-line
  {
  std::stringstream ss(line);
  std::vector<fp_t> new_row; a.push_back(new_row); // add empty new row to the outer vector
  n_cols=0;
  while(ss >> number) // read number-by-number from the row
    {
    a.at(i_row).push_back(number); ++n_cols; // put number to the current row
    if(ss.peek() == ',') ss.ignore(); // if the next token is a comma, ignore it and move on
    }
    n_cols_min=n_cols_min<n_cols ? n_cols_min : n_cols; // check row length
    n_cols_max=n_cols_max>n_cols ? n_cols_max : n_cols; // check row length
  ++i_row;
  }
in_file.close();
return {i_row, n_cols_min, n_cols_max};
}
