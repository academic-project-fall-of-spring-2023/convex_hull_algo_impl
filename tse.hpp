#pragma once

#include <vector> // std::vector
#include <limits> // std::numeric_limits<fp_t>::infinity(), std::numeric_limits<fp_t>::min()
#include <cmath> // std::abs
#include <random> // std::rand(), std::mt19937_64 rng(), std::uniform_real_distribution
#include <chrono> // std::chrono::system_clock::now().time_since_epoch().count()
#include <fstream> // std::ofstream
#include <string> // std::string


namespace tse
{
	template<typename T> void pop_front(std::vector<T>& v) { if (v.size() > 0) v.erase(v.begin()); }


	// Container for 2D points
	template <typename fp_t> class point
	{
		fp_t x; fp_t y;

	public:
		point(fp_t x, fp_t y) { this->x = x; this->y = y; }

		point() { this->x = std::numeric_limits<fp_t>::infinity(); this->y = std::numeric_limits<fp_t>::infinity(); } // the default constructor is introduced technically to enable resize() though resize() in upper_convex_hull() is used solely to shrink the vector

		std::pair<fp_t, fp_t> get_x_y() { return { x,y }; }

		fp_t get_y() const { return y; }
		fp_t get_x() const { return x; }

		bool operator== (const point& p_another) const { return (x == p_another.x && y == p_another.y); }
		bool operator!= (const point& p_another) const { return !(x == p_another.x && y == p_another.y); }

		// output in two-column CSV format without column titles
		friend std::ostream& operator<<(std::ostream& os, const point& p)
		{
			return os << p.x << ", " << p.y;
		}
	}; // class point


	/* Randomly generate a data point set with required number of points and behaviour. Return value is 0 if at least 1 point generated; otherwise -1 */
	template <typename fp_t> int generate_point_test_set(
		std::vector<tse::point<fp_t> >& p, // [I] empty vector on calling; [O] points sorted along (x) on return
		std::pair<long, long> N_series, /* [I] numbers of points to produce: N_first_series, N_second_series.
		  If N_first_series<=0 && N_second_series>=0, then |N_first_series| points have falling (y) and the following (N_second_series) have rising (y).
		  If N_first_series>=0 && N_second_series<=0, then (N_first_series) points have rising (y) and the following |N_second_series| have falling (y).
		  Otherwise |N_first_series+N_second_series| points are generated using random walk along (y) */
		std::tuple<fp_t, fp_t, fp_t, fp_t>& pbb) // [I] preallocated space for [O] resulting point bounding box: x_min,y_min,x_max,y_max
	{
		long N_first_series = N_series.first, N_second_series = N_series.second; // unpack series lengths
		if (N_first_series == N_second_series && N_second_series == 0L) return -1; // no points to generate
		fp_t x = static_cast<fp_t>(0.0), y = x; p.emplace_back(x, y); // the starting point is always (x=0, y=0)
		fp_t x_step, y_min = x, y_max = y; // (x_min) is always 0 and (x_max) is always the last (x) generated

		// prepare uniform random number generators for incremental steps along x and y
		unsigned long long seed = std::chrono::system_clock::now().time_since_epoch().count() + std::rand(); // counts milliseconds
		std::mt19937_64 rng(seed); // randomize seed from the clock
		std::uniform_real_distribution<fp_t> rn_x_step(std::numeric_limits<fp_t>::min(), static_cast<fp_t>(1.0));

		if (N_first_series <= 0L && N_second_series >= 0L) // (y) goes down then up
		{
			std::uniform_real_distribution<fp_t> rn_y_step(std::numeric_limits<fp_t>::min(), static_cast<fp_t>(1.0));
			for (long i = -N_first_series - 1L; i > 0L; --i) // (y) goes down
			{
				do x_step = rn_x_step(rng); while (x + x_step <= x); x += x_step; y -= rn_y_step(rng); p.emplace_back(x, y); y_min = y < y_min ? y : y_min;
			}
			for (long i = N_second_series - (N_first_series == 0L); i > 0L; --i) // (y) goes up
			{
				do x_step = rn_x_step(rng); while (x + x_step <= x); x += x_step; y += rn_y_step(rng); p.emplace_back(x, y); y_max = y > y_max ? y : y_max;
			}
		}
		else if (N_first_series >= 0L && N_second_series <= 0L) // (y) goes up then down
		{
			std::uniform_real_distribution<fp_t> rn_y_step(std::numeric_limits<fp_t>::min(), static_cast<fp_t>(1.0));
			for (long i = N_first_series - 1L; i > 0L; --i) // (y) goes up
			{
				do x_step = rn_x_step(rng); while (x + x_step <= x); x += x_step; y += rn_y_step(rng); p.emplace_back(x, y); y_max = y > y_max ? y : y_max;
			}
			for (long i = -N_second_series - (N_first_series == 0L); i > 0L; --i) // (y) goes down
			{
				do x_step = rn_x_step(rng); while (x + x_step <= x); x += x_step; y -= rn_y_step(rng); p.emplace_back(x, y); y_min = y < y_min ? y : y_min;
			}
		}
		else // (N_first_series) and (N_second_series) share the same sign: (y) performs random drift
		{
			std::uniform_real_distribution<fp_t> rn_y_step(static_cast<fp_t>(-1.0), static_cast<fp_t>(1.0));
			for (long i = std::labs(N_first_series + N_second_series) - 1L; i > 0L; --i)
			{
				do x_step = rn_x_step(rng); while (x + x_step <= x); x += x_step; y += rn_y_step(rng); p.emplace_back(x, y);
				y_min = y < y_min ? y : y_min; y_max = y > y_max ? y : y_max;
			}
		}
		pbb = { static_cast<fp_t>(0.0),y_min,x,y_max }; // (x_max) is always the last x
		return 0;
	} // generate_point_test_set()


	// Converts C-style vectors of point abscissae and ordinates to std::vector<tse::point<fp_t> >.
	template <typename fp_t, typename fp_t_in> inline void x_and_y_C_vectors_to_point_set(
		fp_t_in* px, // [I] pointer to C-style vector of point abscissae
		fp_t_in* py, // [I] pointer to C-style vector of point ordinates
		long N, // [I] length of px[] and py[]
		std::vector<tse::point<fp_t> >& p) // [I] empty vector on calling; [O] points from vectors (x) and (y) on return
	{
		for (long i = 0L; i < N; ++i) p.emplace_back(static_cast<fp_t>(px[i]), static_cast<fp_t>(py[i])); return;
	}


	// Converts std::vector<tse::point<fp_t> > to C-style vectors of point abscissae and ordinates.
	template <typename fp_t, typename fp_t_out> inline void point_set_to_x_and_y_C_vectors(
		fp_t_out* px, // [I] pointer to preallocated for (N) entries C-style vector; [O] vector of point abscissae
		fp_t_out* py, // [I] pointer to preallocated for (N) entries C-style vector; [O] vector of point ordinates
		long& N, // [O] length of px[] and py[]
		std::vector<tse::point<fp_t> >& p) // [I] point set to convert to C-style vectors
	{
		std::pair<fp_t, fp_t> xy; N = p.size();
		for (long i = N - 1L; i >= 0L; --i) { xy = p[i].get_x_y(); px[i] = xy.first; py[i] = xy.second; }
		return;
	}


	/* Compares two point sets. Return values (rv) are:
	rv>=0: index of the first different entry (rv) if the sizes of (p1) and (p2) coincide;
	rv=-1: the sizes of (p1) and (p2) coincide and all entries coincide;
	rv=-2: p1 is shorter (no comparisons made);
	rv=-3: p2 is shorter (no comparisons made). */
	/*template <typename fp_t> inline long compare_point_sets(std::vector<tse::point<fp_t> >& p1, std::vector<tse::point<fp_t> >& p2)
	{
		long i = 0, N1 = p1.size(), N2 = p2.size(), rv = (N1 < N2 ? -2 : (N2 < N1 ? -3 : 0)); if (rv < 0) return rv;
		bool previous_entries_coincide = true;
		while (i < N1 && previous_entries_coincide) { previous_entries_coincide = (p1[i] == p2[i]); ++i; }
		return (previous_entries_coincide && i == N1) ? -1 : --i;
	}*/
	template <typename fp_t> inline long compare_point_sets(std::vector<tse::point<fp_t> >& p1, std::vector<tse::point<fp_t> >& p2)
	{
		long i = 0, N1 = p1.size(), N2 = p2.size(), rv = (N1 < N2 ? -2 : (N2 < N1 ? -3 : 0)); if (rv < 0) return rv;
		bool previous_entries_coincide = true;

		int index1 = 0, index2 = 0;
		double min_x1 = p1[0].get_x(), min_x2 = p2[0].get_x();
		for (int k = 0; k < p1.size(); k++) {
			if (p1[k].get_x() < min_x1) {
				min_x1 = p1[k].get_x();
				index1 = k;
			}

			if (p2[k].get_x() < min_x2) {
				min_x2 = p2[k].get_x();
				index2 = k;
			}
		}

		std::vector<tse::point<fp_t>> p1t, p2t;

		for (int k = index1; k < p1.size() - 1; k++) {
			p1t.push_back(p1[k]);
		}

		for (int k = 0; k < index1; k++) {
			p1t.push_back(p1[k]);
		}

		p1t.push_back(p1[index1]);

		for (int k = index2; k < p2.size() - 1; k++) {
			p2t.push_back(p2[k]);
		}

		for (int k = 0; k < index2; k++) {
			p2t.push_back(p2[k]);
		}

		p2t.push_back(p2[index2]);

		while (i < N1 && previous_entries_coincide) { previous_entries_coincide = (p1t[i] == p2t[i]); ++i; }
		return (previous_entries_coincide && i == N1) ? -1 : --i;
	}


	/* Writes point set to a numerical-only CSV file: every row in the file corresponds to a point: first comes (x), then (y).
	return values: 0 on success; -1 if unable to open the file */
	template <typename fp_t> inline int write_point_set_to_csv_file_without_column_names(
		std::string file_name, // [I] file name to write CSV data to
		std::vector<tse::point<fp_t> >& p) // [I] point set to write
	{
		std::ofstream points_file;
		points_file.open(file_name);
		if (!points_file.is_open()) return -1;
		for (auto a_point : p) points_file << a_point << std::endl;
		points_file.close(); return 0;
	}


	// Extracts points belonging to the lower convex hull. Both end points necessarily belong to it.
	// Return value is 0 if there are at least 2 points in the convex hull; otherwise 1.
	template <typename fp_t> int lower_convex_hull(
		const std::vector<tse::point<fp_t> >& p, // points sorted along (x) to find their lower convex hull
		std::vector<tse::point<fp_t> >& le) // an empty vector to place lower convex hull to
	{
		return 0;
	}


	// Extracts points belonging to the upper convex hull. Both end points necessarily belong to it.
	// Return value is 0 if there are at least 2 points in the convex hull; otherwise 1.
	template <typename fp_t> int upper_convex_hull(
		const std::vector<tse::point<fp_t> >& p, // points sorted along (x) to find their upper convex hull
		std::vector<tse::point<fp_t> >& ue) // an empty vector to place upper convex hull to
	{
		return 0;
	}


	/* Jointly extracts points belonging to the lower and the upper convex hulls. Both end points necessarily belong to both convex hulls. Return value is 0 if there are at least 2 points in each convex hull; otherwise 1 */
	template <typename fp_t> int lower_and_upper_convex_hull(
		const std::vector<tse::point<fp_t> >& p, // points sorted along (x) to find their lower and upper convex hulls
		std::vector<tse::point<fp_t> >& le, // an empty vector to place lower convex hull to
		std::vector<tse::point<fp_t> >& ue) // an empty vector to place upper convex hull to
	{
		return 0;
	}
}
