# How to build project on your machine?
1. Generate build files running ```cmake .``` at the project root.
2. Run ```cmake --build .``` to build the project

# Project desription: 
Links to original articles and source code is free to use in [link to documentation(RU)](https://gitlab.com/academic-project-fall-of-spring-2023/convex_hull_algo_impl/-/blob/main/Documentation/ConvexHull2d.docx)

### Team Members(RTU MIREA, bachelor`s, fourth term, academic project):
1. Anastasia Akhmerova – work with the article, first group report
2. Sofia Vlasova – work with the article, second group report
3. Alexey Polyakov – work with the article, second group report, documentation([Chan`s implementation](https://gitlab.com/academic-project-fall-of-spring-2023/convex_hull_algo_impl/-/blob/main/Chan-source.cpp))
4. Nikita Kulakov – files([Main loop testing](https://gitlab.com/academic-project-fall-of-spring-2023/convex_hull_algo_impl/-/blob/main/ConvexHull_TSE.cpp)), archivarius
5. Georgy Mochalov –  programmer, ([Chan`s implementation](https://gitlab.com/academic-project-fall-of-spring-2023/convex_hull_algo_impl/-/blob/main/Chan-source.cpp))
6. Dmitry Yakimov – programmer, ([Chan-Liu`s implementation](https://gitlab.com/academic-project-fall-of-spring-2023/convex_hull_algo_impl/-/blob/main/Chen-Liu-source.cpp))

