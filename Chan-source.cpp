#pragma once
#include <stdlib.h>
#include <vector>
#include "tse.hpp"
#define RIGHT_TURN -1  // CW
#define LEFT_TURN 1  // CCW
#define COLLINEAR 0  // Collinear
#include <chrono>


/// <summary>
/// Cyclic iterator for a vector
/// </summary>
/// <typeparam name="ValueType"> The data type of the elements in the vector </typeparam>
template<typename ValueType>
class CyclicVectorIterator : public
    std::iterator<std::input_iterator_tag, ValueType>
{
private:

public:
    ValueType* ptr;
    ValueType* begin_;
    ValueType* end_;
    CyclicVectorIterator() { ptr = NULL; }
    //CyclicVectorIterator(ValueType* p) { ptr = p; }
    //CyclicVectorIterator(ValueType* p) { ptr = p; }
    //CyclicVectorIterator(const CyclicVectorIterator& other) { begin_ = other.begin_, end_ = other.end_, ptr = other.ptr; }
    //CyclicVectorIterator(std::vector<ValueType>& vec, ValueType* p) { begin_ = &vec.front(), end_ = &vec.back(), ptr = p; }

    CyclicVectorIterator(std::vector<ValueType>& vec, int p) { begin_ = &vec.front(), end_ = &vec.back(), ptr = &vec[p]; }
    CyclicVectorIterator(std::vector<ValueType>& vec, ValueType& elem) { begin_ = &vec.front(), end_ = &vec.back(), ptr = &elem; }

    bool operator!=(CyclicVectorIterator const& other) const {
        return ptr != other.ptr;
    }
    bool operator==(CyclicVectorIterator const& other) const {
        return ptr == other.ptr;
    }

    ValueType& operator*()
    {
        if (ptr != NULL)
            return *ptr;
    }
    CyclicVectorIterator& operator++() {
        (ptr != end_) ? ++ptr : ptr = begin_;
        return *this;
    }
    CyclicVectorIterator& operator++(int v) {
        auto prev_state = *this;
        (ptr != end_) ? ++ptr : ptr = begin_;
        return prev_state;
    }
    CyclicVectorIterator& operator--() {
        (ptr != begin_) ? --ptr : ptr = end_;
        return *this;
    }
    CyclicVectorIterator operator--(int v) {
        auto prev_state = *this;
        (ptr != begin_) ? --ptr : ptr = end_;
        return prev_state;
    }
    CyclicVectorIterator& operator=(const CyclicVectorIterator& it) {
        begin_ = it.begin_, end_ = it.end_, ptr = it.ptr;
        return *this;
    }
    bool operator==(const CyclicVectorIterator& it) {
        return ptr == it.ptr;
    }
    bool operator!=(const CyclicVectorIterator& it) {
        return ptr != it.ptr;
    }
    /// <summary>
    /// The function moves the iterator to another position in another vector
    /// </summary>
    /// <param name="vec">Vector of elements</param>
    /// <param name="p">The index of the element in the vector</param>
    void update(std::vector<ValueType>& vec, int p) { begin_ = &vec.front(), end_ = &vec.back(), ptr = &vec[p]; }
    /// <summary>
    /// The function moves the iterator to another position in another vector
    /// </summary>
    /// <param name="vec">Vector of elements</param>
    /// <param name="elem">Reference to the element</param>
    void update(std::vector<ValueType>& vec, ValueType& elem) { begin_ = &vec.front(), end_ = &vec.back(), ptr = &elem; }

};



/// <summary>
/// Calculating the square of the distance between two points of a set
/// </summary>
/// <typeparam name="fp_t">Data type of point coordinates</typeparam>
/// <param name="p1">The first point for calculations</param>
/// <param name="p2">The second point for calculations</param>
/// <returns>The square of the distance between two points of a set</returns>
template <typename fp_t> fp_t dist(tse::point<fp_t>& p1, tse::point<fp_t>& p2)
{
    return (p1.get_x() - p2.get_x()) * (p1.get_x() - p2.get_x()) + (p1.get_y() - p2.get_y()) * (p1.get_y() - p2.get_y());
}

/*
    Returns orientation of the line joining points p and q and line joining points q and r
    Returns -1 : CW orientation
            +1 : CCW orientation
            0 : Collinear
    @param p: Object of class point aka first point
    @param q: Object of class point aka second point
    @param r: Object of class point aka third tse::point<fp_t>
*/


/// <summary>
///  Returns orientation of the line joining points p and q and line joining points q and r
///  Returns - 1 : CW orientation
///          + 1 : CCW orientation
///            0 : Collinear
/// </summary>
/// <typeparam name="fp_t">Data type of point coordinates</typeparam>
/// <param name="p">The first point for calculations</param>
/// <param name="q">The second point for calculations</param>
/// <param name="r">The third point for calculations</param>
/// <returns></returns>
template <typename fp_t> int orientation(const tse::point<fp_t>& p, const tse::point<fp_t>& q, const tse::point<fp_t>& r)
{
    fp_t val = (q.get_y() - p.get_y()) * (r.get_x() - q.get_x()) - (q.get_x() - p.get_x()) * (r.get_y() - q.get_y());
    return (val != 0) * ((val > 0) ? -1 : 1); // Collinear: 0 or CW: -1 or CCW: 1
}

/*
    Returns  tse::point<fp_t> to which the tangent is drawn from point p.
    Uses a modified Binary Search Algorithm to yield tangent in O(log n) complexity
    @param v: std::vector of objects of class points representing the hull aka the std::vector of hull points
    @param p: Object of class point from where tangent needs to be drawn
*/


/// <summary>
/// Returns  point<fp_t> to which the tangent is drawn from point p.
/// Uses a modified Binary Search Algorithm to yield tangent in O(log n) complexity
/// </summary>
/// <typeparam name="fp_t">Data type of point coordinates</typeparam>
/// <param name="v">Vector of objects of class points representing the hull points</param>
/// <param name="p">Point from where tangent needs to be drawn</param>
/// <returns> Point<fp_t> to which the tangent is drawn from point p.</returns>

template <typename fp_t> tse::point<fp_t>& tangent(std::vector<tse::point<fp_t>>& v, tse::point<fp_t>& p) {
    int l = 0;
    int r = v.size();
    int l_before = orientation(p, v[0], v[v.size() - 1]);
    int l_after = orientation(p, v[0], v[(l + 1) % v.size()]);
    while (l < r) {
        int c = ((l + r) >> 1);
        int c_before = orientation(p, v[c], v[(c - 1) % v.size()]);
        int c_after = orientation(p, v[c], v[(c + 1) % v.size()]);
        int c_side = orientation(p, v[l], v[c]);
        if (c_before != RIGHT_TURN && c_after != RIGHT_TURN)
            return v[c];
        (c_side == LEFT_TURN) && (l_after == RIGHT_TURN || l_before == l_after) || (c_side == RIGHT_TURN && c_before == RIGHT_TURN) ? r = c : l = c;//l = c + 1;
        //l_before = -c_after;
        l_before = orientation(p, v[l], v[(l - 1 > -1 ? l - 1 : v.size() + l - 1)]);
        l_after = orientation(p, v[l], v[(l + 1) % v.size()]);
    }
    return v[l];
}



/*
    Returns the pair of integers representing the Hull # and the point in that Hull to which the point lpoint will be joined
    @param hulls: std::vector containing the hull points for various hulls stored as individual std::vectors.
    @param lpoint: Pair of the Hull # and the leftmost extreme point contained in that hull, amongst all the obtained hulls
*/


/// <summary>
/// The function moves the iterator to the next point to add to the hull
/// </summary>
/// <typeparam name="fp_t">Data type of point coordinates</typeparam>
/// <param name="hulls">Vector containing the hull points for various hulls stored as individual vectors.</param>
/// <param name="nhull">Index of the previous hull</param>
/// <param name="iter">The cyclic operator</param>
template <typename fp_t> void next_hull_pt(std::vector<std::vector<tse::point<fp_t>> >& hulls, int& nhull, CyclicVectorIterator<tse::point<fp_t> >& iter) {
    tse::point<fp_t> p = *iter;
    tse::point<fp_t> next = *(++iter);
    int prev_hull = nhull;
    int t;
    for (int h = 0; h < prev_hull; ++h)
    {
        tse::point<fp_t>& r = tangent(hulls[h], p);
        t = orientation(p, next, r);
        if (t == RIGHT_TURN || (t == COLLINEAR) && dist(p, r) > dist(p, next))
        {
            next = r;
            nhull = h;
            iter.update(hulls[h], r);
        }
    }
    for (int h = prev_hull + 1; h < hulls.size(); ++h)
    {
        tse::point<fp_t>& r = tangent(hulls[h], p);
        t = orientation(p, next, r);
        if (t == RIGHT_TURN || (t == COLLINEAR) && dist(p, r) > dist(p, next))
        {
            next = r;
            nhull = h;
            iter.update(hulls[h], r);
        }
    }
    return;
}
/*
    Constraint to find the outermost boundary of the points by checking if the points lie to the left otherwise adding the given point p
    Returns the Hull Points
    @param v: std::vector of all the points
    @param p: New point p which will be checked to be in the Hull Points or not
*/

/// <summary>
/// Adds a point while preserving the convexity
/// </summary>
/// <typeparam name="fp_t">Data type of point coordinates</typeparam>
/// <param name="v">Intermediate vector of points for Graham Scan algorithm </param>
/// <param name="p">The point being considered for addition</param>
template <typename fp_t> void keep_left(std::vector<tse::point<fp_t>>& v, const tse::point<fp_t>& p) {
    while (v.size() > 1 && orientation(v[v.size() - 2], v[v.size() - 1], p) != LEFT_TURN)
        v.pop_back();
    if (!v.size() || v[v.size() - 1] != p)
        v.push_back(p);
}

/*
    Graham Scan algorithm to find convex hull from the given set of points
    @param points: List of the given points in the cluster (as obtained by Chan's Algorithm grouping)
    Returns the Hull Points in a std::vector
*/

/// <summary>
/// Implementation of the Graham Scan algorithm to find convex hull from the given set of points
/// </summary>
/// <typeparam name="fp_t">Data type of point coordinates</typeparam>
/// <param name="begin">Iterator indicating the beginning of the range of points for which the hull is being built</param>
/// <param name="end">Iterator indicating the end of the range of points for which the hull is being built</param>
/// <param name="temp_upper_hull">Temporary vector for storing the upper part of the hull</param>
/// <param name="output">An empty vector that will contain the full hull</param>
template <typename fp_t> void GrahamScan(typename std::vector< tse::point<fp_t> >::const_iterator& begin, typename std::vector< tse::point<fp_t>>::const_iterator& end, std::vector<tse::point<fp_t> >& temp_upper_hull, std::vector<tse::point<fp_t> >& output) {
    if (end - begin > 1)
    {
        for (auto i = begin; i < end; ++i)
            keep_left(output, (*i));
        for (auto i = end - 1; i > begin; --i)
            keep_left(temp_upper_hull, (*i));
        keep_left(temp_upper_hull, (*begin));

        for (int i = 1; i < temp_upper_hull.size() - 1; ++i)
            output.emplace_back(temp_upper_hull[i]);
    }
    else { output.assign(begin, end); }
}

/*
    Implementation of Chan's Algorithm to compute Convex Hull in O(nlogh) complexity
*/


/// <summary>
///  Implementation of Chan's Algorithm to compute Convex Hull in O(nlogh) complexity
/// </summary>
/// <typeparam name="fp_t"> Data type of point coordinates</typeparam>
/// <param name="p">Input vector of points sorted by the abscissa axis</param>
/// <param name="output">An empty vector that will contain the full hull</param>
/// <param name="m">Hyperparameter responsible for the number of sets of points</param>
/// <returns>Returns 1 if convex hull of points was constructed and 0 if Algorithm failed</returns>
template <typename fp_t> int chansalgorithm(const std::vector<tse::point<fp_t> >& p, // points sorted along (x) to find their lower and upper convex hulls
    std::vector<tse::point<fp_t> >& output, int m = 3) {
    output.reserve(m);
    std::vector<tse::point<fp_t> > GS_hull; GS_hull.reserve(m);
    std::vector<tse::point<fp_t> > temp; temp.reserve(m);
    std::vector<std::vector<tse::point<fp_t> > > hulls; hulls.reserve(std::ceil(p.size() / m));
    typename std::vector<tse::point<fp_t>>::const_iterator iter_begin = p.begin();
    typename std::vector<tse::point<fp_t>>::const_iterator iter_end = iter_begin;

    size_t p_size = p.size();

    for (int i = 0; i < p_size; i = i + m) {
        iter_end = (i + m <= p_size) ? iter_end + m : p.end();
        GrahamScan(iter_begin, iter_end, temp, GS_hull);
        hulls.push_back(GS_hull);
        GS_hull.resize(0);
        temp.resize(0);
        //iter_begin += m;
        iter_begin += (i + m <= p_size) ? m : 0;
    }
    tse::point<fp_t> first_p = hulls[0][0];
    int nhull = 0;
    int c = 0;
    CyclicVectorIterator<tse::point<fp_t> > iter = CyclicVectorIterator<tse::point<fp_t> >(hulls[0], 0);
    output.push_back(first_p);
    int i = 0;
    do {
        ++i;
        next_hull_pt(hulls, nhull, iter);
        output.emplace_back(*iter);
    } while (i < m && (*iter != first_p));
    if (i == m) { output.resize(0); return 0; }
    return 1;
}
