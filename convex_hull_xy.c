/*//////////////////////////////////////////////////////////////////////////////
// filename: convex_hull_xy.c
// status: tested
// Graham Search method for building convex hull:
// short(est possible ?) implementation manipulating point values
// (C) D.Parfenov May 2004
////////////////////////////////////////////////////////////////////////////////
// implementation notes:
// 1. Every time ulStackPosition points to the place in stack where the last
//    point (which is believed to belong to convex hull) resides.
// 2. (x1,y1),(x2,y2),(x3,y3), x1<x2<x3 are thriade of points to check,
//    whether they form a part of convex hull.
//    If so, the leftmost one (x1,y1) is added to the stack and the triad is
//           shifted one position to the right;
//    If not, the middle one (x2,y2) is eliminated and if stack is not empty
//           the last "good" one is retrieved from the stack, otherwise the
//           next (x3,y3) will be taken
// 3. The functions build_upper_convex_hull_xy and build_lower_convex_hull_xy
//    have the only difference: sign in checking of point thriade
//////////////////////////////////////////////////////////////////////////////*/




/* computes upper convex hull */
void build_upper_convex_hull_xy
(
double *x, double *y, /* [I] arrays with abscissae and ordinatas of the points, x[i+1]>x[i] assumed */
unsigned long n, /* [I] the length of x, y, xCH, and yCH; n>=3 */
double *xCH, double *yCH, /* [O] pointer to the preallocated vectors (not less than n elements each).
                             On exit, abscissae and ordinatas of convex hull points are written in
                             xCH[0] ... xCH[*pnCH] and yCH[0] ... yCH[*pnCH] correspondingly;
                             x[i+1]>x[i] is guaranteed */
unsigned long *pnCH /* [O] index of the last convex hull point in pulStack */
)
{
double x1, y1, x2, y2, x3, y3;
register signed long k3=2, slStackPosition=-1; /* stack is empty */

x1=*x; y1=*y; x2=*(x+1); y2=*(y+1);
do
  {
  x3=*(x+k3); y3=*(y+k3);
  if ((x1-x2)*(y3-y2)>=(x3-x2)*(y1-y2)) /* check the middle point in thriade */
    {
    /* add a knot to stack */
    slStackPosition++; *(xCH+slStackPosition)=x1; *(yCH+slStackPosition)=y1;
    x1=x2; y1=y2; x2=x3; y2=y3; k3++;
    }
  else
    {
    if (slStackPosition>=0)
      {
      /* remove a knot from stack */
      x2=x1; y2=y1;
      x1=*(xCH+slStackPosition); y1=*(yCH+slStackPosition); slStackPosition--;
      }
    else { x2=x3; y2=y3; k3++; } /* stack is empty: take the next point */
    }
  }
while (k3<n);

/* push last two knots in the stack */
*(xCH+(++slStackPosition))=x1; *(yCH+slStackPosition)=y1;
*(xCH+(++slStackPosition))=x2; *(yCH+slStackPosition)=y2;
*pnCH=slStackPosition;
return;
}




/* computes lower convex hull */
void build_lower_convex_hull_xy
(
double *x, double *y, /* [I] arrays with abscissae and ordinatas of the points, x[i+1]>x[i] assumed */
unsigned long n, /* [I] the length of x, y, xCH, and yCH; n>=3 */
double *xCH, double *yCH, /* [O] pointer to the preallocated vectors (not less than n elements each).
                             On exit, abscissae and ordinatas of convex hull points are written in
                             xCH[0] ... xCH[*pnCH] and yCH[0] ... yCH[*pnCH] correspondingly;
                             x[i+1]>x[i] is guaranteed */
unsigned long *pnCH /* [O] index of the last convex hull point in pulStack */
)
{
double x1, y1, x2, y2, x3, y3;
register signed long k3=2, slStackPosition=-1; /* stack is empty */

x1=*x; y1=*y; x2=*(x+1); y2=*(y+1);
do
  {
  x3=*(x+k3); y3=*(y+k3);
  if ((x1-x2)*(y3-y2)<=(x3-x2)*(y1-y2)) /* check the middle point in thriade */
    {
    /* add a knot to stack */
    slStackPosition++; *(xCH+slStackPosition)=x1; *(yCH+slStackPosition)=y1;
    x1=x2; y1=y2; x2=x3; y2=y3; k3++;
    }
  else
    {
    if (slStackPosition>=0)
      {
      /* remove a knot from stack */
      x2=x1; y2=y1;
      x1=*(xCH+slStackPosition); y1=*(yCH+slStackPosition); slStackPosition--;
      }
    else { x2=x3; y2=y3; k3++; } /* stack is empty: take the next point */
    }
  }
while (k3<n);

/* push last two knots in the stack */
*(xCH+(++slStackPosition))=x1; *(yCH+slStackPosition)=y1;
*(xCH+(++slStackPosition))=x2; *(yCH+slStackPosition)=y2;
*pnCH=slStackPosition;
return;
}
