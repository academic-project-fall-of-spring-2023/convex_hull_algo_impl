import random
import time
import tkinter.messagebox
from tkinter import *
import os
import subprocess
import shlex

#path to the exe file
path = "../x64/debug/ConvexHull_TSE.exe"

class Window(Tk):
    def __init__(self):
        super().__init__()
        self.resizable(height=False, width=False)
        self.title("Test Framework")
        self.lbl1 = Label(self, text="Number of tests:", font=(0, 14))
        self.lbl1.grid(column=0, row=0, padx=4, pady=4)
        self.n_test_txt = Entry(self, font=(0, 14), justify="center")
        self.n_test_txt.grid(column=1, row=0, padx=4, pady=4)
        self.lbl2 = Label(self, text="A min:", font=(0, 14))
        self.lbl2.grid(column=0, row=1, padx=4, pady=4)
        self.a_min_txt = Entry(self, font=(0, 14), justify="center")
        self.a_min_txt.grid(column=1, row=1, padx=4, pady=4)
        self.lbl3 = Label(self, text="A max:", font=(0, 14))
        self.lbl3.grid(column=2, row=1, padx=4, pady=4)
        self.a_max_txt = Entry(self, font=(0, 14), justify="center")
        self.a_max_txt.grid(column=3, row=1, padx=4, pady=4)
        self.lbl4 = Label(self, text="B min:", font=(0, 14))
        self.lbl4.grid(column=0, row=2, padx=4, pady=4)
        self.b_min_txt = Entry(self, font=(0, 14), justify="center")
        self.b_min_txt.grid(column=1, row=2, padx=4, pady=4)
        self.lbl5 = Label(self, text="B max:", font=(0, 14))
        self.lbl5.grid(column=2, row=2, padx=4, pady=4)
        self.b_max_txt = Entry(self, font=(0, 14), justify="center")
        self.b_max_txt.grid(column=3, row=2, padx=4, pady=4)

        self.btn1 = Button(self, text="Run test", width=20, font=(0, 14), command=self.btn1_clk)
        self.btn1.grid(column=1, row=3, pady=10, padx=4)

    def btn1_clk(self):
        try:
            n_test = int(self.n_test_txt.get())
            a_min = int(self.a_min_txt.get())
            a_max = int(self.a_max_txt.get())
            b_min = int(self.b_min_txt.get())
            b_max = int(self.b_max_txt.get())
        except:
            tkinter.messagebox.showerror("Error!", "Invalid entry")

        if n_test <= 0:
            tkinter.messagebox.showerror("Error!", "Number of tests must be positive")

        elif a_max < a_min:
            tkinter.messagebox.showerror("Error!", "A max must be lower then A min")

        elif b_max < b_min:
            tkinter.messagebox.showerror("Error!", "B max must be lower then B min")

        else:
            cmd = shlex.split(str(path + f" {n_test} {a_min} {a_max} {b_min} {b_max}"))
            subprocess.Popen(cmd)
w = Window()
w.mainloop()


