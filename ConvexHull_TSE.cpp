/* The primary build:
gcc -c convex_hull_xy.c -o convex_hull_xy.o
g++ -c compare_tse_to_convex_hull_2004.cpp -o compare_tse_to_convex_hull_2004.o
g++ -o c.exe compare_tse_to_convex_hull_2004.o convex_hull_xy.o

The consequent build after changes in compare_tse_to_convex_hull_2004.cpp only:
g++ -o c.exe compare_tse_to_convex_hull_2004.cpp convex_hull_xy.o

This frameworks compares results of lower and upper convex hull extraction algorithms:
a) TSE 2023, separate lower and upper convex hull extractor,
b) TSE 2023 joint lower and upper convex hull extractor, and
c) Graham scan, implementation 2004 as well as

To do so, it automatically generates point test sets with required properties. */
#include <chrono>
#include <tuple> // std::tuple, std::get
#include <iostream> // std::cerr
#include <fstream> // std::ofstream
#include <cassert> // assert
#include "tse.hpp"
#include "Chan-source.cpp"
#include "Chen-Liu-point-source.h"
#include "Chen-Liu-source.h"


extern "C" void build_lower_convex_hull_xy(double*, double*, unsigned long, double*, double*, unsigned long*);
extern "C" void build_upper_convex_hull_xy(double*, double*, unsigned long, double*, double*, unsigned long*);

typedef double fp_t;

int main(int argc, char *argv[])
{
    // input data section /////////////////////////////////////////////////////////////////////////////

    // Intervals of kinds and lengths of the first and the second parts of the point sequence; details see generate_point_test_set()
    long N_tests = 1;
    long N_first_series_min = 3;
    long N_first_series_max = 10;
    long N_second_series_min = 3;
    long N_second_series_max = 10;

    // Override by command prompt arguments
    if (argc == 6) {
        std::vector<long> arguments;
        arguments.reserve(5);
        for (int i = 1; i < argc; ++i) arguments.push_back(std::stol(argv[i]));

        N_tests = arguments[0];
        N_first_series_min = arguments[1];
        N_first_series_max = arguments[2];
        N_second_series_min = arguments[3];
        N_second_series_max = arguments[4];
    }


    // CSV file name to write raw test points to, if a problem arises:
    std::string file_name_input = "./points.csv";

    // CSV file name to write generated points of lower_convex_hull() to, if a problem arises:
    //std::string file_name_output_le_2023 = "./le_2023.csv";
    // CSV file name to write generated points of lower convex hull from lower_and_upper_convex_hull() to, if a problem arises:
    //std::string file_name_output_jle_2023 = "./jle_2023.csv";
    // CSV file name to write generated points of build_lower_convex_hull_xy() to, if a problem arises:
    //std::string file_name_output_le_2004 = "./le_2004.csv";

    // CSV file name to write generated points of upper_convex_hull() to, if a problem arises:
    //std::string file_name_output_ue_2023 = "./ue_2023.csv";
    // CSV file name to write generated points of upper convex hull from lower_and_upper_convex_hull() to, if a problem arises:
    //std::string file_name_output_jue_2023 = "./jue_2023.csv";
    // CSV file name to write generated points of build_upper_convex_hull_xy() to, if a problem arises:
    //std::string file_name_output_ue_2004 = "./ue_2004.csv";

    // CSV file name to write generated points of convex hull using Graham scan (2004) to, if a problem arises:
    std::string file_name_output_hull_2004 = "./hull_2004.csv";
    // CSV file name to write generated points of convex hull using LiuChen's alogorithm (2023) to, if a problem arises:
    std::string file_name_output_hull_2023_1 = "./hull_2023_1.csv";
    // CSV file name to write generated points of convex hull using Chan's alogorithm (2023) to, if a problem arises:
    std::string file_name_output_hull_2023_2 = "./hull_2023_2.csv";

    //command to run python script for visualization of hulls
    std::string command = "python ConvexHull.py";



    // Use auto keyword to avoid typing long
    // type definitions to get the timepoint
    // at this instant use function now()
    for (int a = N_first_series_min; a< N_first_series_max; ++a)
    { 
        for (int b = N_second_series_min; b< N_second_series_max; ++b)
        {
            long N_first_series = a;
            long N_second_series = b;
            // code section 
            long N = std::labs(N_first_series) + std::labs(N_second_series), N_a;
            if (N < 3) { std::cerr << "\nbuild_lower_convex_hull_xy() and build_upper_convex_hull_xy() are not applicable to point sets with N<3" << std::endl; assert(0); }
            unsigned long N_unsigned_long, N_le_2004_unsigned_long, N_ue_2004_unsigned_long; // for interfacing build_upper_convex_hull_xy()
            int rv1, rv2, rv_; // return values

            std::vector<tse::point<fp_t> > p; p.reserve(N); // raw point set for testing
            std::tuple<fp_t, fp_t, fp_t, fp_t> pbb; // point bounding box for (p): x_min, y_min, x_max, y_max
            //double x[N], y[N], x_le[N], y_le[N], x_ue[N], y_ue[N];
            double* x = new double[N];
            double* y = new double[N];
            double* x_le = new double[N];
            double* y_le = new double[N];
            double* x_ue = new double[N];
            double* y_ue = new double[N];
            long nextcout_n = 0;
            long long total_time_1 = 0;
            long long total_time_2 = 0;
            long long total_time_3 = 0;
            for (long n_test = 0; n_test < N_tests; ++n_test)
            {
                if (rv_ = tse::generate_point_test_set(p, { N_first_series, N_second_series }, pbb))
                {
                    std::cerr << "\ngenerate_point_test_set: no test points generated, rv_=" << rv_ << std::endl; assert(0);
                }

                
                // begin of Graham scan, implementation 2004 
                // convert TSE compatible input point set format to build_lower_convex_hull_xy() and build_upper_convex_hull_xy() compatible input point set format
                auto start1 = std::chrono::high_resolution_clock::now();
                tse::point_set_to_x_and_y_C_vectors<fp_t, double>(&x[0], &y[0], N_a, p); // N_a does not empty itself
                if (N_a != N)
                {
                    std::cerr << "\npoint_set_to_x_and_y_C_vectors: (N_a=" << N_a << ")!=(N=" << N << ")\n"; assert(0);
                }
                N_unsigned_long = static_cast<unsigned long>(N);
                build_lower_convex_hull_xy(&x[0], &y[0], N_unsigned_long, &x_le[0], &y_le[0], &N_le_2004_unsigned_long); // lower convex hull computed using Graham scan, implementation 2004
                build_upper_convex_hull_xy(&x[0], &y[0], N_unsigned_long, &x_ue[0], &y_ue[0], &N_ue_2004_unsigned_long); // upper convex hull computed using Graham scan, implementation 2004
                // convert build_lower_convex_hull_xy() and build_upper_convex_hull_xy() output to TSE compatible format
                std::vector<tse::point<fp_t> > hull_2004; // where to place the lower convex hull computed using Graham scan, implementation 2004
                std::vector<tse::point<fp_t> > ue_2004; // where to place the upper convex hull computed using Graham scan, implementation 2004
                tse::x_and_y_C_vectors_to_point_set<fp_t, double>(x_le, y_le, static_cast<long>(N_le_2004_unsigned_long), hull_2004);
                tse::x_and_y_C_vectors_to_point_set<fp_t, double>(x_ue, y_ue, static_cast<long>(N_ue_2004_unsigned_long), ue_2004);
                // end of Graham scan, implementation 2004 and its wrappers /////////////////////////////////////
                hull_2004.emplace_back(p[p.size() - 1]);
                for (long i = ue_2004.size() - 1; i >= 0; --i) hull_2004.emplace_back(ue_2004[i]);
                auto stop1 = std::chrono::high_resolution_clock::now();
                auto duration1 = std::chrono::duration_cast<std::chrono::microseconds>(stop1 - start1);


                //LiuChen's Algorithm
                auto start2 = std::chrono::high_resolution_clock::now();
                std::vector<tse::point<fp_t> > hull_2023_1;
                point* p_arr = new point[p.size()];
                for (int i = 0; i < p.size(); i++) {
                    point tmp;
                    tmp.x = p[i].get_x();
                    tmp.y = p[i].get_y();

                    p_arr[i] = tmp;
                }
                OuelletHull h(p_arr, p.size(), true);
                int t = hull_2004.size();
                point* p_res = h.GetResultAsArray(t);
                for (int i = 0; i < t; i++) {
                    tse::point<fp_t> tmp(p_res[i].x, p_res[i].y);
                    hull_2023_1.emplace_back(tmp);
                }
                auto stop2 = std::chrono::high_resolution_clock::now();
                auto duration2 = std::chrono::duration_cast<std::chrono::microseconds>(stop2 - start2);


                //Chan's Algorithm
                auto start3 = std::chrono::high_resolution_clock::now();
                std::vector<tse::point<fp_t> > hull_2023_2;
                chansalgorithm(p, hull_2023_2, hull_2004.size());
                auto stop3 = std::chrono::high_resolution_clock::now();
                auto duration3 = std::chrono::duration_cast<std::chrono::microseconds>(stop3 - start3);

                //Definition of return values
                rv1 = tse::compare_point_sets(hull_2023_1, hull_2004);
                rv2 = tse::compare_point_sets(hull_2023_2, hull_2004);
                if ((rv1 != -1) || (rv2 != -1)) // -1 means the sizes of (hull_2023_1) and (hull_2004) coincide and all their entries coincide
                {
                    if (rv_ = tse::write_point_set_to_csv_file_without_column_names(file_name_input, p) != 0)
                    {
                        std::cerr << "\nwrite_point_set_to_csv_file_without_column_names: unable to write file \"" << file_name_input << "\"\n"; assert(0);
                    }
                    if (rv_ = tse::write_point_set_to_csv_file_without_column_names(file_name_output_hull_2004, hull_2004) != 0)
                    {
                        std::cerr << "\nwrite_point_set_to_csv_file_without_column_names: unable to write file \"" << file_name_output_hull_2004 << "\"\n"; assert(0);
                    }
                    if (rv_ = tse::write_point_set_to_csv_file_without_column_names(file_name_output_hull_2023_1, hull_2023_1) != 0)
                    {
                        std::cerr << "\nwrite_point_set_to_csv_file_without_column_names: unable to write file \"" << file_name_output_hull_2023_1 << "\"\n"; assert(0);
                    }
                    if (rv_ = tse::write_point_set_to_csv_file_without_column_names(file_name_output_hull_2023_2, hull_2023_2) != 0)
                    {
                        std::cerr << "\nwrite_point_set_to_csv_file_without_column_names: unable to write file \"" << file_name_output_hull_2023_2 << "\"\n"; assert(0);
                    }

                    if (rv1 == -2)
                    {
                        #ifdef _WIN32
                            _popen(command.c_str(), "r");
                        #else
                            popen(command.c_str(), "r");
                        #endif
                        std::cerr << "\ncompare_point_sets: hull_2023_1 is shorter than hull_2004; both are written to files" << std::endl; assert(0);
                    }
                    if (rv1 == -3)
                    {
                        #ifdef _WIN32
                            _popen(command.c_str(), "r");
                        #else
                            popen(command.c_str(), "r");
                        #endif
                        std::cerr << "\ncompare_point_sets: hull_2004 is shorter than hull_2023_1; both are written to files" << std::endl; assert(0);
                    }

                    if (rv2 == -2)
                    {
                        #ifdef _WIN32
                            _popen(command.c_str(), "r");
                        #else
                            popen(command.c_str(), "r");
                        #endif
                        std::cerr << "\ncompare_point_sets: hull_2023_2 is shorter than hull_2004; both are written to files" << std::endl; assert(0);
                    }
                    if (rv2 == -3)
                    {
                        #ifdef _WIN32
                            _popen(command.c_str(), "r");
                        #else
                            popen(command.c_str(), "r");
                        #endif
                        std::cerr << "\ncompare_point_sets: hull_2004 is shorter than hull_2023_2; both are written to files" << std::endl; assert(0);
                    }
                    #ifdef _WIN32
                        _popen(command.c_str(), "r");
                    #else
                        popen(command.c_str(), "r");
                    #endif
                    std::cerr << "\ncompare_point_sets: unknown return value rv1=" << rv1 << ", rv2=" << rv2 << std::endl; assert(0);
                }
                total_time_1 += duration1.count();
                total_time_2 += duration2.count();
                total_time_3 += duration3.count();
                p.resize(0);
            }
            delete[] x;
            delete[] y;
            delete[] x_le;
            delete[] y_le;
            delete[] x_ue;
            delete[] y_ue;
            std::cout << "\nDone successfully: N_tests=" << N_tests << ", all with N_first_series=" << N_first_series << ", N_second_series=" << N_second_series << std::endl;
            std::cout << "Average execution times:" << "\nGraham Scan: " << total_time_1 / N_tests << "\nLiuChen's Algorithm: " << total_time_2 / N_tests << "\nChan's Algorithm: " << total_time_3 / N_tests << std::endl;
        }
    }
}