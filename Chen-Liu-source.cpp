/**
	\file Main file
	\brief Contains one function to do all the work
*/

//#include "stdafx.h"
#include "Chen-Liu-source.h"
#include "Chen-Liu-point-source.h"
#include <string.h>
#include <thread>
#include <future>

point *ouelletHull(point *pArrayOfPoint, int count, bool closeThePath, int &resultCount) {
    OuelletHull convexHull(pArrayOfPoint, count, closeThePath);
    return convexHull.GetResultAsArray(resultCount);
}

/**
*   \brief Calculate convex hull
*
*   \param[in] void
*   \return void
*/
void OuelletHull::CalcConvexHull() {
// Find the quadrant limits (maximum x and y)
    point *pPt = _pPoints;

    point q1p1 = *pPt;
    point q1p2 = *pPt;
    point q2p1 = *pPt;
    point q2p2 = *pPt;
    point q3p1 = *pPt;
    point q3p2 = *pPt;
    point q4p1 = *pPt;
    point q4p2 = *pPt;

    pPt++;

    for (int n = _countOfPoint - 1; n > 0; n--) {
        point &pt = *pPt;

// Right
        if (pt.x >= q1p1.x) {
            if (pt.x == q1p1.x) {
                if (pt.y > q1p1.y) {
                    q1p1 = pt;
                } else {
                    if (pt.y < q4p2.y) {
                        q4p2 = pt;
                    }
                }
            } else {
                q1p1 = pt;
                q4p2 = pt;
            }
        }

// Left
        if (pt.x <= q2p2.x) {
            if (pt.x == q2p2.x) {
                if (pt.y > q2p2.y) {
                    q2p2 = pt;
                } else {
                    if (pt.y < q3p1.y) {
                        q3p1 = pt;
                    }
                }
            } else {
                q2p2 = pt;
                q3p1 = pt;
            }
        }

// Top
        if (pt.y >= q1p2.y) {
            if (pt.y == q1p2.y) {
                if (pt.x < q2p1.x) {
                    q2p1 = pt;
                } else {
                    if (pt.x > q1p2.x) {
                        q1p2 = pt;
                    }
                }
            } else {
                q1p2 = pt;
                q2p1 = pt;
            }
        }

// Bottom
        if (pt.y <= q3p2.y) {
            if (pt.y == q3p2.y) {
                if (pt.x < q3p2.x) {
                    q3p2 = pt;
                } else {
                    if (pt.x > q4p1.x) {
                        q4p1 = pt;
                    }
                }
            } else {
                q3p2 = pt;
                q4p1 = pt;
            }
        }

        pPt++;
    }

    point q1rootPt = {
            q1p2.x, q1p1.y
    };
    point q2rootPt = {q2p1.x, q2p2.y};
    point q3rootPt = {q3p2.x, q3p1.y};
    point q4rootPt = {q4p1.x, q4p2.y};

// Q1 Init
    q1hullCapacity = _quadrantHullPointArrayInitialCapacity;
    q1pHullPoints = new point[q1hullCapacity];

    q1pHullPoints[0] = q1p1;
    if (compare_points(q1p1, q1p2))  //
    {
        q1hullCount = 1;
    } else {
        q1pHullPoints[1] = q1p2;
        q1hullCount = 2;
    }

// Q2 Init
    q2hullCapacity = _quadrantHullPointArrayInitialCapacity;
    q2pHullPoints = new point[q2hullCapacity];

    q2pHullPoints[0] = q2p1;
    if (compare_points(q2p1, q2p2)) {
        q2hullCount = 1;
    } else {
        q2pHullPoints[1] = q2p2;
        q2hullCount = 2;
    }

// Q3 Init
    q3hullCapacity = _quadrantHullPointArrayInitialCapacity;
    q3pHullPoints = new point[q3hullCapacity];

    q3pHullPoints[0] = q3p1;
    if (compare_points(q3p1, q3p2)) {
        q3hullCount = 1;
    } else {
        q3pHullPoints[1] = q3p2;
        q3hullCount = 2;
    }

// Q4 Init
    q4hullCapacity = _quadrantHullPointArrayInitialCapacity;
    q4pHullPoints = new point[q4hullCapacity];

    q4pHullPoints[0] = q4p1;
    if (compare_points(q4p1, q4p2)) {
        q4hullCount = 1;
    } else {
        q4pHullPoints[1] = q4p2;
        q4hullCount = 2;
    }

// Start Calc

// Calc per quadrant
    int index;
    int indexLow;
    int indexHi;

//Point part
    pPt = _pPoints;
  
    auto t1 = std::async(std::launch::async, &OuelletHull::StartCheck, this, std::ref(q1rootPt), 1, std::ref(q1hullCount), std::ref(q1hullCapacity), std::ref(q1pHullPoints), std::ref(pPt));
    auto t2 = std::async(std::launch::async, &OuelletHull::StartCheck, this, std::ref(q2rootPt), 2, std::ref(q2hullCount), std::ref(q2hullCapacity), std::ref(q2pHullPoints), std::ref(pPt));
    auto t3 = std::async(std::launch::async, &OuelletHull::StartCheck, this, std::ref(q3rootPt), 3, std::ref(q3hullCount), std::ref(q3hullCapacity), std::ref(q3pHullPoints), std::ref(pPt));
    auto t4 = std::async(std::launch::async, &OuelletHull::StartCheck, this, std::ref(q4rootPt), 4, std::ref(q4hullCount), std::ref(q4hullCapacity), std::ref(q4pHullPoints), std::ref(pPt));

    t1.get();
    t2.get();
    t3.get();
    t4.get();
}

void OuelletHull::StartCheck(point& qrootPt, short ptnum, int& qhullCount, int& qhullCapacity, point* qHullPoints, point* pPt) {
    for (int n = _countOfPoint - 1; n >= 0; n--) {
        point& pt = *pPt;

        isInQ(pt, qrootPt, ptnum, qhullCount, qhullCapacity, qHullPoints, pPt);

        pPt++;
    }
}
/*! \brief 

 \param[in,out] pPoint
 \param[in] Index �
 \param[in] 
 \param[in] 
 \param[in] 
 \return void

 
*/
void OuelletHull::InsertPoint(point *&pPoint, int index, point &pt, int &count, int &capacity) {
// make some room to insert the point. make sure to not reach capacity and/or adjust it
    if (count >= capacity) {
// Should make some room
//int newCapacity = capacity + _quadrantHullPointArrayGrowSize; // Very bad in the worse case. Fallback to regular way of growing list capacity
        int newCapacity = capacity * 2;
        point *newPointArray = new point[newCapacity];
        memmove(newPointArray, pPoint, capacity * sizeof(point));
        delete pPoint;
        pPoint = newPointArray;
        capacity = newCapacity;
    }

    memmove(&(pPoint[index + 1]), &(pPoint[index]), (count - index) * sizeof(point));

// Insert Point at index
    pPoint[index] = pt;
    count++;
}

void OuelletHull::isInQ(point& pt, point& qrootPt, short ptnum, int& qhullCount, int& qhullCapacity, point* qHullPoints, point* pPt) {
    int index;
    int indexLow;
    int indexHi;
    int xsign = (ptnum == 1 || ptnum == 4) ? 1 : -1;
    int ysign = (ptnum == 1 || ptnum == 2) ? 1 : -1;

    if (xsign * pt.x > xsign * qrootPt.x && ysign * pt.y > ysign * qrootPt.y) {
        indexLow = 0;
        indexHi = qhullCount;

        while (indexLow < indexHi - 1) {
            index = ((indexHi - indexLow) >> 1) + indexLow;

            if (xsign * pt.x <= xsign * qHullPoints[index].x && ysign * pt.y <= ysign * qHullPoints[index].y) {
                return;
            }

            if (ysign * pt.x > ysign * qHullPoints[index].x) {
                indexHi = index;
                continue;
            }

            if (ysign * pt.x < ysign * qHullPoints[index].x) {
                indexLow = index;
                continue;
            }

            indexLow = index - 1;
            indexHi = index + 1;
            break;
        }

        if (!right_turn(qHullPoints[indexLow], qHullPoints[indexHi], pt)) {
            return;
        }   

        while (indexLow > 0) {
            if (right_turn(qHullPoints[indexLow - 1], pt, qHullPoints[indexLow])) {
                break;
            }

            indexLow--;
        }

        int maxIndexHi = qhullCount - 1;
        while (indexHi < maxIndexHi) {
            if (right_turn(pt, qHullPoints[indexHi + 1], qHullPoints[indexHi])) {
                break;
            }

            indexHi++;
        }

        if (indexLow + 1 == indexHi) {
            InsertPoint(qHullPoints, indexLow + 1, pt, qhullCount, qhullCapacity);
            return;
        } else if (indexLow + 2 == indexHi) {
            qHullPoints[indexLow + 1] = *pPt;
            return;
        } else {
            qHullPoints[indexLow + 1] = *pPt;
            RemoveRange(qHullPoints, indexLow + 2, indexHi - 1, qhullCount);
            return;
        }
    }

    return;
}


/// Remove every item in from index start to indexEnd inclusive
void OuelletHull::RemoveRange(point *pPoint, int indexStart, int indexEnd, int &count) {
    memmove(&(pPoint[indexStart]), &(pPoint[indexEnd + 1]), (count - indexEnd) * sizeof(point));
    count -= (indexEnd - indexStart + 1);
}

//! 
OuelletHull::OuelletHull(point *points, int countOfPoint, bool shouldCloseTheGraph) {
    _pPoints = points;
    _countOfPoint = countOfPoint;
    _shouldCloseTheGraph = shouldCloseTheGraph;

    CalcConvexHull();
}

/// Destructor
OuelletHull::~OuelletHull() {
    delete q1pHullPoints;
    delete q2pHullPoints;
    delete q3pHullPoints;
    delete q4pHullPoints;
}

/* \brief GetResultAsArray

  \param[in] 
 \return point*
*/
point *OuelletHull::GetResultAsArray(int &hullPointCount) {
    hullPointCount = 0;
    if (this->_countOfPoint == 0) {
        return NULL;
    }

    unsigned int indexQ1Start;
    unsigned int indexQ2Start;
    int indexQ3Start;
    int indexQ4Start;
    int indexQ1End;
    int indexQ2End;
    int indexQ3End;
    int indexQ4End;

    indexQ1Start = 0;
    indexQ1End = q1hullCount - 1;
    point pointLast = q1pHullPoints[indexQ1End];

    if (q2hullCount == 1) {
        if (compare_points(*q2pHullPoints, pointLast)) {
            indexQ2Start = 1;
            indexQ2End = 0;
        } else {
            indexQ2Start = 0;
            indexQ2End = 0;
            pointLast = *q2pHullPoints;
        }
    } else {
        if (compare_points(*q2pHullPoints, pointLast)) {
            indexQ2Start = 1;
        } else {
            indexQ2Start = 0;
        }
        indexQ2End = q2hullCount - 1;
        pointLast = q2pHullPoints[indexQ2End];
    }

    if (q3hullCount == 1) {
        if (compare_points(*q3pHullPoints, pointLast)) {
            indexQ3Start = 1;
            indexQ3End = 0;
        } else {
            indexQ3Start = 0;
            indexQ3End = 0;
            pointLast = *q3pHullPoints;
        }
    } else {
        if (compare_points(*q3pHullPoints, pointLast)) {
            indexQ3Start = 1;
        } else {
            indexQ3Start = 0;
        }
        indexQ3End = q3hullCount - 1;
        pointLast = q3pHullPoints[indexQ3End];
    }

    if (q4hullCount == 1) {
        if (compare_points(*q4pHullPoints, pointLast)) {
            indexQ4Start = 1;
            indexQ4End = 0;
        } else {
            indexQ4Start = 0;
            indexQ4End = 0;
            pointLast = *q4pHullPoints;
        }
    } else {
        if (compare_points(*q4pHullPoints, pointLast)) {
            indexQ4Start = 1;
        } else {
            indexQ4Start = 0;
        }

        indexQ4End = q4hullCount - 1;
        pointLast = q4pHullPoints[indexQ4End];
    }

    if (compare_points(q1pHullPoints[indexQ1Start], pointLast)) {
        indexQ1Start++;
    }

    int countOfFinalHullPoint = (indexQ1End - indexQ1Start) +
                                (indexQ2End - indexQ2Start) +
                                (indexQ3End - indexQ3Start) +
                                (indexQ4End - indexQ4Start) + 4; //< index of the last point



    if (countOfFinalHullPoint <=
        1) // Case where there is only one point or many of only the same point. Auto closed if required.
    {
        return new point[1]{
                pointLast
        };
    }

    if (countOfFinalHullPoint > 1 && _shouldCloseTheGraph) {
        countOfFinalHullPoint++;
    }

    point *results = new point[countOfFinalHullPoint];

    int resIndex = 0;

    for (int n = indexQ3Start; n <= indexQ3End; n++) {
        results[resIndex] = q3pHullPoints[n];
        resIndex++;
    }

    for (int n = indexQ4Start; n <= indexQ4End; n++) {
        results[resIndex] = q4pHullPoints[n];
        resIndex++;
    }

    for (int n = indexQ1Start; n <= indexQ1End; n++) {
        results[resIndex] = q1pHullPoints[n];
        resIndex++;
    }

    for (int n = indexQ2Start; n <= indexQ2End; n++) {
        results[resIndex] = q2pHullPoints[n];
        resIndex++;
    }

    if (countOfFinalHullPoint > 1 && _shouldCloseTheGraph) {
        results[resIndex] = results[0];
    }

    hullPointCount = countOfFinalHullPoint;

    return results;
}


