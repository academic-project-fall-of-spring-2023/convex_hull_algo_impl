/*! 
*  \file OuelletHull.h
* \brief Header file for 
*
*/
#pragma once

#include <locale.h>
#include <math.h>
#include "Chen-Liu-point-source.h"



/*!
*  \class OuelletHull algorithm 
* 
* 
*  Implementation of Lui and Chen with virtual quadrant idea
*  \public OuelletHull - constructor
*  \public GetResultAsArray
*  \param Count? 
*  \return array of points for Convex Hull
*/
class OuelletHull {
private:
    static const int _quadrantHullPointArrayInitialCapacity = 1000;
    static const int _quadrantHullPointArrayGrowSize = 1000;

    point *_pPoints;
    int _countOfPoint;
    bool _shouldCloseTheGraph;

    point *q1pHullPoints;
    point *q1pHullLast;
    int q1hullCapacity;
    int q1hullCount = 0;

    point *q2pHullPoints;
    point *q2pHullLast;
    int q2hullCapacity;
    int q2hullCount = 0;

    point *q3pHullPoints;
    point *q3pHullLast;
    int q3hullCapacity;
    int q3hullCount = 0;

    point *q4pHullPoints;
    point *q4pHullLast;
    int q4hullCapacity;
    int q4hullCount = 0;

    void CalcConvexHull();
    void StartCheck(point& qrootPt, short ptnum, int& qhullCount, int& qhullCapacity, point* qHullPoints, point* pPt);
    void isInQ(point& pt, point& qrootPt, short ptnum, int& qhullCount, int& qhullCapacity, point* qHullPoints, point* pPt);

    inline static void InsertPoint(point *&pPoint, int index, point &pt, int &count, int &capacity);

    inline static void RemoveRange(point *pPoint, int indexStart, int indexEnd, int &count);

public:
    OuelletHull(point *points, int countOfPoint, bool shouldCloseTheGraph = true);

    ~OuelletHull();

    point *GetResultAsArray(int &count);
};

point *ouelletHull(point *pArrayOfPoint, int count, bool closeThePath, int &resultCount);
