import numpy as np
import matplotlib.pyplot as plt

fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
fig.suptitle('Convex Hull Comparison')
ax1.set_title("LiuChan's Algorithm")
ax2.set_title('Graham Scan')
ax3.set_title("Chan's Algorithm")
point_set = np.genfromtxt('points.csv', delimiter=',')

#LiuChensAlgorithm
ax1.scatter(point_set[:, 0], point_set[:, 1], c='black')
convhull = np.genfromtxt('hull_2023_1.csv', delimiter=',')
ax1.scatter(convhull[:, 0], convhull[:, 1], c='red')
ax1.plot(convhull[:, 0], convhull[:, 1], c='red')

#Graham Scan
ax2.scatter(point_set[:, 0], point_set[:, 1], c='black')
convhull = np.genfromtxt('hull_2004.csv', delimiter=',')
ax2.scatter(convhull[:, 0], convhull[:  , 1], c='green')
ax2.plot(convhull[:, 0], convhull[:, 1], c='green')

#ChansAlgorithm
ax3.scatter(point_set[:, 0], point_set[:, 1], c='black')
convhull = np.genfromtxt('hull_2023_2.csv', delimiter=',')
ax3.scatter(convhull[:, 0], convhull[:, 1], c='blue')
ax3.plot(convhull[:, 0], convhull[:, 1], c='blue')

plt.show()     